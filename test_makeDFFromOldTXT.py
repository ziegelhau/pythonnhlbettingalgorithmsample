import inspect
import os
from timeit import default_timer as timer

import pandas as pd

from make_df_from_old_txt import makeDFFromOldTXT


# This whole test is a little redundant because we have to basically use the function to test it
# Unless I'm going to write the dataframe out by hand
def test_makeDFFromOldTXT():
    # relative import from menu_options.py
    test_dir = "text/unit/" + inspect.stack()[0][3] + "/"
    txt_filename = test_dir + "in_df.txt"
    header_filename = test_dir + "header.txt"
    correct_df = test_dir + "correct_df.txt"
    max_time_to_run = 5  # whatever
    correct_df = pd.read_csv(os.path.join(correct_df), sep="_", header=0)
    correct_df['Date'] = pd.to_datetime(pd.Series(correct_df["Date"], index=correct_df.index))

    start = timer()
    df = makeDFFromOldTXT(txt_filename, header_filename)
    end = timer()
    elapsed = end - start
    assert elapsed < max_time_to_run
    assert df.equals(correct_df)