import inspect
from timeit import default_timer as timer

import pandas as pd

from make_df_from_old_txt import makeDFFromOldTXT
from make_team_df_from_raw_player_stats import _calculateAveragesPlayer
from moving_average import MovingAverage


def test_calculateAveragesPlayer():
    # Check that calculateAveragesPlayer works with simple
    # moving average of window size 2 (new value = (old value + old value 2)/2)
    root_test_dir = "text/unit/" + inspect.stack()[0][3]
    test_dir = root_test_dir + "/short/"
    max_time_to_run = 7  # whatever
    in_header = test_dir + "header.txt"
    in_data = test_dir + "in_df.txt"
    technique = MovingAverage({"window": 2, "axis": 0, "min_periods": 1})
    correct_df_filename = test_dir + "correct_df.txt"

    correct_df = pd.read_csv(correct_df_filename, sep="_", header=0)
    correct_df["Date"] = pd.to_datetime(correct_df["Date"])
    data = makeDFFromOldTXT(in_data, in_header)
    start = timer()
    avgs_up_to_game = _calculateAveragesPlayer(data, technique, "Player")
    end = timer()
    elapsed = end - start
    assert elapsed < max_time_to_run
    # Just checked this one by hand to confirm that is was working
    # Then copied the results to the correct_df
    assert avgs_up_to_game.equals(correct_df)