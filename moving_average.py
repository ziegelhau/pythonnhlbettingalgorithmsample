from technique_base_class import TechniqueBaseClass


# Calculates the moving average of a dataframe given the rolling parameters
# Window should be 1 to align with the math we were doing before
# Want to just change to a function, and just pass that in
class MovingAverage(TechniqueBaseClass):
    """
    Improvements
        - Enum for dictionary keys
        - Change to function maybe?
            - Not 100% sure that this needs to be a class
    """
    # Set default values in case I forget what they should be
    vals = {"window": 8, "axis": 0, "min_periods": 1}

    def __init__(self, in_dict):
        self.vals = in_dict

    def strategy(self, dataframe):
        # Update values in user decided to pass in values during strategy call
        # https://stackoverflow.com/questions/13996302/python-rolling-functions-for-groupby-object
        return dataframe.rolling(window=self.vals["window"],
                                 axis=self.vals["axis"], min_periods=self.vals["min_periods"]).mean()

        # other option
        # https: // stackoverflow.com / questions / 13728392 / moving - average - or -running - mean
        # modes = ['full', 'same', 'valid']
        # return np.convolve(dataset, np.ones((past_n_days,)) / past_n_days, mode = mode)

    def __eq__(self, other):
        if not isinstance(other, MovingAverage):
            # don't attempt to compare against unrelated types
            return NotImplemented

        return self.vals == other.vals