from make_df_from_old_txt import makeDFFromOldTXT


def makeTeamDFFromRawPlayerStats(player_stats_filename, header_filename, averaging_technique):
    """    
    :param player_stats_filename: File with player stats delimited by "_"
    :param header_filename: Contains header for player_stats_df
    :param averaging_technique:
    :param shift_games: Bool, shift games up 1 value to make average_up_to_game instead of average_up_to_and_including_game
    :return: df of team averages up to and not including that game
    """

    player_stats_unsorted = makeDFFromOldTXT(player_stats_filename, header_filename)
    player_stats = player_stats_unsorted.sort_values(["Date"])

    # Note: calculateAveragesPlayer also sorts the data but am worried that I will forget about it sometime
    # So am currently doing it twice
    player_avgs_df = _calculateAveragesPlayer(player_stats, averaging_technique, "Player")
    team_avgs_df = _makeTeamAverageFromPlayerStats(player_avgs_df)
    team_avgs_df_with_2_indices = team_avgs_df.set_index("Team", append=True)
    team_avgs_df_with_2_indices = team_avgs_df_with_2_indices.reorder_levels(["Team", "Date And Opponent"])
    # Need to sort the index by team and date so that when we shift the values it is done correctly
    team_avgs_df_sorted = team_avgs_df_with_2_indices.sort_index()
    return team_avgs_df_sorted


def _calculateAveragesPlayer(player_stats, technique, key):
    """
    Given raw player stats, apply the averaging technique "technique"
    To the stats and return the average player stats UP TO AND INCLUDING THAT GAME
    :param player_stats: Player stats df
    :param technique: Averaging technique
    :param key: Has to be "Player" currently
    :return:

    Improvements
        - Generalize function to work with team stats as well
        - Should be able to specify key as "Team" for it to work
        - Will need to be able to drop string columns
            - Think that not all the player stats have the same columns (or same order? Can't remember)
    """
    """
    Average can be found by finding getting the unique list of players (item) in the stats
    Then find each row of data matching that item and calculate the average for that person
    Then append to a dataframe having all the averages and returning it
    """
    player_stats = player_stats.sort_values(["Date"])
    player_stats_only = player_stats.drop(["Date", "Team", "Position", "Date And Opponent", "Opponent"], axis=1)
    grouped = player_stats_only.groupby(key)
    # https://stackoverflow.com/questions/13996302/python-rolling-functions-for-groupby-object
    # Apply averaging technique
    player_averages = grouped.apply(lambda x: technique.strategy(x))
    # Add in the date and date and opponent columns
    player_averages[["Date", "Date And Opponent"]] = player_stats[["Date", "Date And Opponent"]]
    # update the values with the averaged values
    player_stats.update(player_averages)
    return player_stats


def _makeTeamAverageFromPlayerStats(player_df):
    """
    :param player_df: Dataframe containing all player stats
    :return: All played stats summed and cumulative sum for games played
    """
    grouped_df = player_df.groupby(["Date And Opponent"])
    sum_df = grouped_df.sum()
    # Create team dataframe so we can add it back in after doing the sum
    # Create copy to remove reference to original dataframe
    team = player_df[["Team", "Date And Opponent"]].copy()
    team = team.drop_duplicates()
    team = team.set_index("Date And Opponent")
    sum_df["Team"] = team["Team"]

    # Get cumulative sum of games played to replace the sum of games played
    sum_df["Games Played"] = _getCumGamesPlayedDF(player_df)["Games Played"]
    return sum_df


def _getCumGamesPlayedDF(in_df):
    """
    :param in_df: Dataframe with player stats
    :return: dataframe containing only Games Played, with rows index = Date And Opponent
    """
    games_played = in_df[["Player", "Games Played"]].copy()
    player_group = games_played.groupby("Player")
    games_played_cumsum = player_group.cumsum()
    # Add back in the "Date And Opponent" column
    games_played_cumsum["Date And Opponent"] = in_df["Date And Opponent"]
    date_and_opp_group = games_played_cumsum.groupby("Date And Opponent")
    return date_and_opp_group.mean()