import os

import pandas as pd

from get_data_labels_from_og_file_header import getDataLabelsFromOGFileHeader


# These guys were saying it was better to just have a module with different functions in this case
# Because there's no advantage to making an instance of a class here so just don't do it
# https://stackoverflow.com/questions/68645/are-static-class-variables-possible-in-python
def _readTXTFileToDF(txt_filename, header_filename):
    df = pd.read_csv(os.path.join(txt_filename), sep="_", header=None)
    df = df.drop(df.columns[[-1, ]], axis=1)  # Drop the last blank column
    header = getDataLabelsFromOGFileHeader(header_filename)
    df.columns = header
    return df


def _addDateAndTeamColumns(df):
    # TODO, strip whitespace here
    # Will break aaaaaaaaaal my tests
    # make optional flag to add stats
    # Could also add, longhand opponent, shorthand opponent, shorthand team, longhand team
    df["Date"], df["Opponent"] = df["Date And Opponent"].str.split(" vs | @ ", 1).str
    # Convert to datetime because is currently a string
    df["Date"] = pd.to_datetime(df["Date"].str.replace("/", "-"))
    return df


def makeDFFromOldTXT(txt_filename, header_filename):
    """
    :param txt_filename: Text file containing the "_" delimited stats file
    :param header_filename: Name of file containing header
    :return: Dataframe of text_file with "Date" and "Opponent" added

    Improvements
        - Specify flag so we can choose don't have to add the extra columns if we don't want to
    """
    df = _readTXTFileToDF(txt_filename, header_filename)
    df = _addDateAndTeamColumns(df)
    return df