import os


def getDataLabelsFromOGFileHeader(filename):
    with open(os.path.join(filename), "r") as file:
        data_labels = file.read().split("_")
    return data_labels