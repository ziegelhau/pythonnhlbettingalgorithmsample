# Given raw player stats, apply the averaging technique "technique"
# To the stats and return the averaged stats at each date
# Key is either "Team" or "Player <- TODO make more clear
import pandas as pd


# TODO Change name to be more general
# Average can be found by finding getting the unique list of teams or players (item) in the stats
# Then find each row of data matching that item and calculate the average for that person
# Then append to a dataframe having all the averages and returning it
def calculateAveragesPlayer(player_stats, technique, key):
    all_player_avgs = pd.DataFrame(columns=player_stats.columns)

    teams_or_players = pd.unique(player_stats[key])

    for team_or_player in teams_or_players:
        single_player_with_labels = pd.DataFrame(player_stats.loc[(player_stats[key] == team_or_player)])

        # TODO This should be a function itself which we unit test that it limits the data correctly
        single_player_no_labels = single_player_with_labels.copy()

        single_player_no_labels.drop(["Date", "Player", "Team", "Position", "Date And Opponent"], inplace=True, axis=1)

        technique.vals["dataframe"] = single_player_no_labels.copy()

        single_player_avg = technique.strategy()

        # Updates the columns in player averages with the columns that match in single_player_avg
        # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.update.html
        single_player_with_labels.update(single_player_avg)

        all_player_avgs = all_player_avgs.append(single_player_with_labels)

    return all_player_avgs