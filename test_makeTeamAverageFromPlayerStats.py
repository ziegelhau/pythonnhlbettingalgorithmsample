import inspect
import os
from timeit import default_timer as timer

import pandas as pd

from make_df_from_old_txt import makeDFFromOldTXT
from make_team_df_from_raw_player_stats import _makeTeamAverageFromPlayerStats


def test_makeTeamAverageFromPlayerStats():
    test_dir = "text/unit/" + inspect.stack()[0][3] + "/"
    txt_filename = test_dir + "in_df.txt"
    header_filename = test_dir + "header.txt"
    correct_df_filename = test_dir + "correct_df.txt"
    max_time_to_run = 3  # whatever

    correct_df = pd.read_csv(os.path.join(correct_df_filename), sep="_", header=0, index_col=0)
    in_player_stats = makeDFFromOldTXT(txt_filename, header_filename)

    start = timer()
    all_team_matchups = _makeTeamAverageFromPlayerStats(in_player_stats)
    end = timer()
    elapsed = end - start
    assert elapsed < max_time_to_run
    assert all_team_matchups.equals(correct_df)