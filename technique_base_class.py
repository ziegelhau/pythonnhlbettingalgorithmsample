from abc import ABC


class TechniqueBaseClass(ABC):
    # @abstractmethod
    def strategy(self):
        pass