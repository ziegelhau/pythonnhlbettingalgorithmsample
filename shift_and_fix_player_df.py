def shiftAndFixPlayerDF(df):
    """
    Need to set the first games to 0 when making the input matrix for training
    :param df:
    :return:
    """
    # Teams = list of teams in the dataframe
    # For grouped by players currently
    teams = df.index.levels[0].tolist()
    df = df.shift(1)
    # TODO: probably improvement to just find Nans and replace with 0?
    # df = df.fillna(0), bit more error prone as may replace NaN value that needs fixed in a different way
    for team in teams:
        df.loc[team].loc[df.loc[team, :].iloc[0].name] = 0

    return df