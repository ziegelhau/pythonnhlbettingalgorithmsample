import pandas as pd

from moving_average import MovingAverage


def test_MovingAverage():
    """
    Test could be a little more thorough but am confident it's working
    """
    MA = MovingAverage({"dataframe": pd.DataFrame({"a": [1, 3]}), "window": 2, "axis": 0, "min_periods": 1})
    correct_df = pd.DataFrame({"a": [1.0, 2.0]})
    assert correct_df.equals(MA.strategy(MA.vals["dataframe"]))

    MA.vals = {"dataframe": pd.DataFrame({"a": [1, 3, 5]}), "window": 3, "axis": 0, "min_periods": 1}
    correct_df = pd.DataFrame({"a": [1.0, 2.0, 3.0]})
    assert correct_df.equals(MA.strategy(MA.vals["dataframe"]))

    MA.vals = {"dataframe": pd.DataFrame({"a": [1], "b": [3]}), "window": 2, "axis": 1, "min_periods": 1}
    correct_df = pd.DataFrame({"a": [1.0], "b": [2.0]})
    assert correct_df.equals(MA.strategy(MA.vals["dataframe"]))

    MA.vals = {"dataframe": pd.DataFrame({"a": [1.0, 2.0, 3.0, 4.0, 5.0]}), "window": 3, "axis": 0, "min_periods": 1}
    correct_df = pd.DataFrame({"a": [1.0, 1.5, 2.0, 3.0, 4.0]})
    assert correct_df.equals(MA.strategy(MA.vals["dataframe"]))