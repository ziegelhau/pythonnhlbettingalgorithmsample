import inspect
import os
from timeit import default_timer as timer

import pandas as pd

from make_df_from_old_txt import makeDFFromOldTXT
from make_team_df_from_raw_player_stats import _getCumGamesPlayedDF


def test__getCumGamesPlayedDF():
    test_dir = "text/unit/" + inspect.stack()[0][3] + "/"
    txt_filename = test_dir + "in_df.txt"
    header_filename = test_dir + "header.txt"
    correct_df_filename = test_dir + "correct_df.txt"
    max_time_to_run = 5  # whatever

    correct_df = pd.read_csv(os.path.join(correct_df_filename), sep="_", header=0, index_col=0)
    df = makeDFFromOldTXT(txt_filename, header_filename)
    start = timer()
    cum_games_played_df = _getCumGamesPlayedDF(df)
    end = timer()
    elapsed = end - start
    assert cum_games_played_df.equals(correct_df)
    assert elapsed < max_time_to_run